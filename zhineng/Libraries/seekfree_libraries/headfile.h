/*********************************************************************************************************************
* COPYRIGHT NOTICE
* Copyright (c) 2019,逐飞科技
* All rights reserved.
* 技术讨论QQ群：一群：179029047(已满)  二群：244861897
*
* 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
* 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
*
* @file				headfile
* @company			成都逐飞科技有限公司
* @author			逐飞科技(QQ3184284598)
* @version			查看doc内version文件 版本说明
* @Software			IAR 8.3 or MDK 5.28
* @Target core		MM32F3277
* @Taobao			https://seekfree.taobao.com/
* @date				2021-02-22
********************************************************************************************************************/
 
#ifndef _headfile_h
#define _headfile_h


#include <stdint.h>

#include "board.h"
#include "SEEKFREE_PRINTF.h"


//------逐飞科技单片机外设驱动头文件
#include "zf_adc.h"
#include "zf_camera.h"
#include "zf_exti.h"
#include "zf_flash.h"
#include "zf_gpio.h"
#include "zf_spi.h"
#include "zf_systick.h"
#include "zf_pit.h"
#include "zf_pwm.h"
#include "zf_tim.h"
#include "zf_uart.h"
#include "zf_fsmc.h"

//------逐飞科技产品驱动头文件
#include "SEEKFREE_18TFT.h"
#include "SEEKFREE_FONT.h"
#include "SEEKFREE_OLED.h"
#include "SEEKFREE_IIC.h"
#include "SEEKFREE_MPU6050.h"

//------哈士乾の代码
//#include "vofa.h"
#include "control.h"
#include "tim.h"
#include "motor.h"
#include "pwm.h"
#include "MovingFilter.h"
#include "nomalize.h"
#include "allinit.h"
#include "math.h"
#include "test.h"
#include "scheduler.h"
#include "key.h"
#include "ui.h"
#include "imu.h"
#include "elect.h"
#include "ultrasonic.h"



//-------变量声明
extern int Motor_A,Motor_B,Servo,Target_A,Target_B;  //电机PWM变量 应是Motor的 向Moto致敬	
extern int16 flag_knock,flag_lcd;
extern float Gyro_kp,Real_kp,Real_kd;
extern float Pitch,Yaw,Roll;
extern float Velocity_KP,Velocity_KI,exp_turn_KP;	
extern int Encoder_Left,Encoder_Right;
extern int V_forkget,V_forkin,V_ring;
extern float Velocity,Velocity_Set,Turn,Velocity_MAX;
extern int PWM_MAX,PWM_MIN;
extern float ADC_L,ADC_LM,ADC_RM,ADC_R;
extern float TURN_A,TURN_B;
extern float ADC_LL,ADC_L,ADC_LM,ADC_RM,ADC_R,ADC_RR;//电感采集值
extern int beep;
extern  u8 ring_state,fork_state,stop_state,ramp_state;
extern u8 ring_state,fork_state,stop_state;
extern float err;
extern float sum_kd;
extern float Turn_KP,Turn_KD;
extern int wave_get;
#endif

