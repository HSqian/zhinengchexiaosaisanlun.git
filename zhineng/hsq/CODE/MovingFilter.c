#include "MovingFilter.h"

float movingfilter(ADCN_enum adc, ADCCH_enum ch)
{
    static int buf[NUM];
    static int index=0,flag=0;
    static float sum=0;
    int adc_get=adc_mean_filter(adc,ch,1);

    sum+=adc_get-buf[index];
    buf[index]=adc_get;
    index++;

    if(index==NUM)
    {
        index=0;
        flag=1;
    }

    if(flag==0) return sum/index;
    else        return sum/NUM;
}