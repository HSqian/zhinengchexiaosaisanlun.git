#ifndef __MovingFilter_H
#define __MovingFilter_H

#include "headfile.h"

float movingfilter(ADCN_enum adc, ADCCH_enum ch);

#define NUM 5

#endif