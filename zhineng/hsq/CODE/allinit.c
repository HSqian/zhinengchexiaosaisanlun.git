#include "allinit.h"

int Motor_A,Motor_B,Servo,Target_A,Target_B;
int16 flag_knock,flag_lcd;
float Velocity_KP=20,Velocity_KI=0.01;
float Gyro_kp=10,Real_kp=10,Real_kd=0.05;
float Turn_KP=20,Turn_KD=0,exp_turn_KP=0;
int Encoder_Left=0,Encoder_Right=0;
int V_forkget=0,V_forkin=0,V_ring=0;
float Velocity,Velocity_Set,Turn,Velocity_MAX=0;
int PWM_MAX=4500,PWM_MIN=-2000;//pwm限幅
float ADC_LL,ADC_L,ADC_LM,ADC_RM,ADC_R,ADC_RR;//电感采集值
float Pitch = 0,Yaw = 0,Roll = 0;
float TURN_A=0,TURN_B=1;
char buf[8];

void all_init(void)
{
    lcd_init();

    myadc_init();
    Encoder_Init();
    Motor_PWM_Init();
    Scheduler_Init();
    KEY_Init();
    uart_init(UART_4,115200,UART4_TX_C10,UART4_RX_C11);
    board_init(true);
    gpio_init(B11,GPO, GPIO_HIGH, GPO_PUSH_PULL);


//	uart_init(UART_1,115200,UART1_TX_A09,UART1_RX_A10);

//------标志位
    flag_lcd=1;

    flag_knock=1;
}