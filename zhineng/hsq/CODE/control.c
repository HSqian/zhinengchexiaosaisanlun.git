#include "control.h"


u8 Flag_Target;
int Voltage_Temp,Voltage_Count,Voltage_All,sum;
int adc[6][8];
float err;
float sum_kd;//补偿积分器

float u;
/**************************************************************************
函数功能：小车运动数学模型
入口参数：速度和转角
返回  值：无
**************************************************************************/
void Kinematic_Analysis(float velocity,float turn)
{
    float  x=-exp_turn_KP;
    float temp_velocity;
    temp_velocity=x*GFP_abs(err*100)+velocity;
    Target_A=(temp_velocity-turn);
    Target_B=(temp_velocity+turn);      //后轮差速
}

/**************************************************************************
函数功能：异常关闭电机
入口参数：电压
返回  值：1：异常  0：正常
**************************************************************************/
u8 Turn_Off( int voltage)
{
    u8 temp;
    if(0)//电池电压低于7.4V关闭电机
    {
        temp=1;
    }
    else
        temp=0;
    return temp;
}


/**************************************************************************
函数功能：增量PI控制器
入口参数：编码器测量值，目标速度
返回  值：电机PWM
根据增量式离散PID公式
pwm+=Kp[e（k）-e(k-1)]+Ki*e(k)+Kd[e(k)-2e(k-1)+e(k-2)]
e(k)代表本次偏差
e(k-1)代表上一次的偏差  以此类推
pwm代表增量输出
在我们的速度控制闭环系统里面，只使用PI控制
pwm+=Kp[e（k）-e(k-1)]+Ki*e(k)
**************************************************************************/
int Incremental_PI_A (int Encoder,int Target)
{
    static int Bias,Pwm,Last_bias;
    Bias=Target-Encoder;                //计算偏差

    Pwm+=Velocity_KP*(Bias-Last_bias)+Velocity_KI*Bias;   //增量式PI控制器

    Last_bias=Bias;	                   //保存上一次偏差

    return Pwm;                         //增量输出

}
int Incremental_PI_B (int Encoder,int Target)
{

    static int Bias,Pwm,Last_bias;
    Bias=Target-Encoder;

    Pwm+=Velocity_KP*(Bias-Last_bias)+Velocity_KI*Bias;

    Last_bias=Bias;	                   //保存上一次偏差

    return Pwm;                         //增量输出
}


/**************************************************************************
函数功能：控制位置式pid
入口参数：无
返回  值：无
pwm+=Kp*e（k）+Ki*e(k)+Kd[e(k)-e(k-1)]
**************************************************************************/
void Get_RC(void)
{
    static float Last_Bias=0,exp_turn=0,Bias=0,real_Bais=0,real_last_bias=0;
    float gyro_z,real_gyro_z;

    gyro_z=mpu_gyro_z/2000;            //实际角速度的提取
    my_adcfilter_get();

    ramp_stable();
    fork_stable();
    ring_stable();
    stop_stable();

    err=err_get(ADC_L,ADC_LM,ADC_RM,ADC_R,TURN_A,TURN_B);
    Bias=err*100;
    exp_turn=Turn_KP*err+Turn_KD*(Bias-Last_Bias);
    Last_Bias=Bias;                                     //期望角速度的计算（通过差比和的结果进行pid处理得到）

    real_gyro_z=gyro_z*Gyro_kp;   //实际角速度的计算

    real_Bais=exp_turn-real_gyro_z;
    Turn=Real_kp*real_Bais+Real_kd*(real_Bais-real_last_bias);
    real_last_bias=real_Bais;//用实际角速度串期望角速度，使转向平滑

//    expturn_Bias=0-err;  //提取偏差
//    Turn=((GFP_abs(Bias)*Bias*0.008+Bias*0.02+(Bias-Last_Bias)*3)); //
//	  exp_turn=exp_turn_KP*expturn_Bias;
//	  Bias=exp_turn-Gyro.y;
}


/********************
函数功能：六路adc滑动滤波（滑动滤波个der，均值滤波好用）
入口参数：无
返  回值：无
********************/

void my_adcfilter_get(void)
{
    int i=0,j=0;

    for(j=0; j<8; j++)
    {
        adc[0][j] =adc_mean_filter(ADC_2,ADC2_CH04_A04,5);
        adc[1][j] =adc_mean_filter(ADC_2,ADC2_CH05_A05,5);
        adc[2][j] =adc_mean_filter(ADC_2,ADC2_CH06_A06,5);
        adc[3][j] =adc_mean_filter(ADC_2,ADC2_CH07_A07,5);
        adc[4][j] =adc_mean_filter(ADC_2,ADC2_CH14_C04,5);
        adc[5][j] =adc_mean_filter(ADC_2,ADC2_CH15_C05,5);
    }

    for(j=0; j<8; j++)
    {
        ADC_RR=adc[0][j];
        ADC_R =adc[1][j];
        ADC_RM=adc[2][j];
        ADC_LM=adc[3][j];
        ADC_L =adc[4][j];
        ADC_LL=adc[5][j];

        ADC_LL =nomal(ADC_LL,LLRR_MAX);
        ADC_L	 =nomal(ADC_L,LR_MAX);
        ADC_LM =nomal(ADC_LM,LMRM_MAX);
        ADC_RM =nomal(ADC_RM,LMRM_MAX);
        ADC_R  =nomal(ADC_R,LR_MAX);
        ADC_RR =nomal(ADC_RR,LLRR_MAX);
    }
}


