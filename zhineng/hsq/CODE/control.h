#ifndef __CONTROL_H
#define __CONTROL_H
#include "headfile.h"

#define PI 3.14159265



void Kinematic_Analysis(float velocity,float turn);
void TIM1_UP_IRQHandler(void) ;
int Incremental_PI_A (int Encoder,int Target);
int Incremental_PI_B (int Encoder,int Target);
void Get_RC(void);
void my_adcfilter_get(void);


typedef struct PID
{
    float SumError;		// ?��2?��???
    int32 LastError;	// Error[-1]
    int32 PrevError;	// Error[-2]
    int32 LastData;		// Speed[-1]
} PID;

int32 PlacePID_Control(PID *sprt, float *PID, int32 NowPiont, int32 SetPoint);
#endif