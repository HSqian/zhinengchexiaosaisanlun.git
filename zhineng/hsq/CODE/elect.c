#include "elect.h"

/**********************
文件说明：标志位判断
每个控制没有太大问题
需要针对不同的电磁环境
多多少少修改下标志位的参数
**********************/
enum Ring_state
{
    ring_noring=0,
    ring_prering,
    ring_ring,
    ring_leftring,
    ring_rightring,
    ring_ringin,
    ring_out,
    ring_realout,
};

enum Fork_state
{
    fork_start=0,
    fork_out,
    fork_wave,
    fork_get,
    fork_in,
    fork_stop,
};

enum Ramp_state
{
    ramp_noramp=0,
    ramp_getramp,
};

enum Stop_state
{
    stop_nostop=0,
    stop_outstop,
};

u8 ring_state=0;
u8 fork_state=0;
u8 stop_state=0;
u8 ramp_state=0;




void ramp_stable (void)
{
    int static a_encoder=0;
    switch(ramp_state)
    {

    case ramp_noramp:
    {
        if(ring_state==ring_noring&&Pitch>12)
        {
            ramp_state=ramp_getramp;
        }
    };
    break;

    case ramp_getramp:
    {
        a_encoder+=GFP_abs(Encoder_Left);
        if(a_encoder>40000)
        {
            ramp_state=ramp_noramp;
            a_encoder=0;
        }
    };
    break;
    }

}



void fork_stable(void)
{
    int Motor_C;
    int static A_R=0,A_L=0;

    switch(fork_state)
    {

    case fork_start :
    {
        Velocity=V_forkin;
        if(ADC_RM>30)
            fork_state=fork_out;
    }
    break;

    case fork_out :
    {
//        if(ring_state==ring_noring)
//        {
            Velocity=Velocity_MAX;

                if(wave_get==1)
                {
                    fork_state=fork_wave;
                }
            
//        }
    };
    break;

    case fork_wave :
    {
				Velocity=150;
        A_R+=GFP_abs(Encoder_Right);
        A_L+=GFP_abs(Encoder_Left);

        if(A_R>20000||A_L>20000)
        {
            Velocity=Velocity_MAX;
            fork_state=fork_out;
            A_R=0,A_L=0;
        }
				
				 if(ADC_RM+ADC_LM<70)
        {
            fork_state=fork_get;
        }

    };
    break;
		
    case fork_get :
    {
       
        ADC_LM=ADC_RM+80;
        A_R+=Encoder_Right;
        if(A_R>20000)
        {
            fork_state=fork_in;
            A_R=0;
        }
    };
    break;

    case fork_in:
    {
        A_R+=Encoder_Right;
        A_L+=Encoder_Left;
        if(A_R>=40000||A_L>40000)
        {
            Velocity=0;
            TURN_A=0;
            TURN_B=0;
            fork_state=fork_stop;
            uart_putchar(UART_4,0x5B);
        }
    };
    break;
    }
}


void ring_stable(void)
{
    int static a_l=0;
    switch(ring_state)
    {

    case ring_noring:
    {
        if((ADC_LM==100&&ADC_LM+ADC_RM>160)||(ADC_RM==100&&ADC_LM+ADC_RM>160))
            ring_state=ring_prering;

    };
    break;

    case ring_prering :
    {
        Velocity=V_ring;
        beep=1;
        if(ADC_L>=50||ADC_R>=50)//环岛检测
            ring_state=ring_ring;
    };
    break;

    case ring_ring :
    {
        TURN_A=1;
        TURN_B=0;
        if((ADC_LL-ADC_RR>50)||(ADC_LL>80))
        {
            ring_state=ring_leftring;
        }
//        if((ADC_RR-ADC_LL>50)||ADC_RR>80)
//        {
//            ring_state=ring_rightring;
//        }
        beep=2;
    };
    break;

    case ring_leftring :
    {
        ADC_L=90;
        a_l+=GFP_abs(Encoder_Left);
        if((ADC_L+ADC_R<95||ADC_R<20)&&a_l>5000)
        {
            ring_state=ring_ringin;
            a_l=0;
        }
    };
    break;

    case ring_rightring :
    {
        ADC_R=90;
        if((ADC_L+ADC_R<95||ADC_L<20)&&a_l>5000)
        {
            ring_state=ring_ringin;
            a_l=0;
        }
    };
    break;

    case ring_ringin :
    {
        Velocity=0.9*Velocity_MAX;
        TURN_A=0.1;
        TURN_B=1;
        beep=3;
        if(ADC_LM>90||ADC_RM>90)
            ring_state=ring_out;
    };
    break;

    case ring_out :
    {
        beep=4;
        Velocity=Velocity_MAX;
        if(GFP_abs(ADC_RM-ADC_LM)>10&&ADC_RM<90)
        {
            ring_state=ring_realout;
        }
        if(GFP_abs(ADC_RM-ADC_LM)>10&&ADC_LM<90)
        {
            ring_state=ring_realout;
        }
    }
    break;

    case ring_realout :
    {
        a_l+=GFP_abs(Encoder_Left);
        if(ADC_LM<90)
        {
            ADC_LM+=5;
        }
        if(ADC_RM<90)
        {
            ADC_RM+=5;
        }
        if(TURN_A!=1&&ADC_LL<35&&a_l>30000)
        {
            ring_state=ring_noring;
            a_l=0;
        }
    };
    break;

    }
}

void stop_stable (void)
{
    switch(stop_state)
    {

    case stop_nostop:
    {
        if((ADC_LM<8&&ADC_RM<8))
        {
            static float integral_encoder=0;
            integral_encoder=integral_encoder+GFP_abs(Encoder_Left);
            if(integral_encoder>=1500)
            {
                stop_state=stop_outstop;
                integral_encoder=0;
            }
        }
    };
    break;

    case stop_outstop:
    {
        Velocity=0;
        TURN_A=0;
        TURN_B=0;
        if(ADC_LL>10&&ADC_LM>10&&ADC_RM>10)
        {
            stop_state=stop_nostop;
            Velocity=Velocity_MAX;
        }
    };
    break;
    }
}
