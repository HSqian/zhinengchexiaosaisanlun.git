#ifndef __ELECT_H
#define __ELECT_H

#include "headfile.h"

void ring_stable(void);
void stop_stable (void);
void fork_stable(void);
void ramp_stable (void);

#endif