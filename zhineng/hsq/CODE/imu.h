/******************************************************************************
* 以下所有内容版权均属蓝电所有，未经允许不得用于商业用途,仅限蓝电内部传播
* @file		 四元数姿态融合
* @date		 2021.4.23
* @school    湖北工业大学
* @Software  MDK5 V5.32
* @author    戴润泽
* @version   V1.0
* @Target    CORE MM32SPIN2XPS
******************************************************************************/
#ifndef _IMU_H_
#define _IMU_H_
#include <math.h>

void MahonyAHRSupdateIMU(float gx, float gy, float gz, float ax, float ay, float az, float* pitch, float* roll, float* yaw);
#endif
