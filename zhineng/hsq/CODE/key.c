#include "key.h"

void KEY_Init(void)
{
    gpio_init(E10,GPI,GPIO_HIGH,GPI_PULL_UP);
    gpio_init(E11,GPI,GPIO_HIGH,GPI_PULL_UP);
    gpio_init(E12,GPI,GPIO_HIGH,GPI_PULL_UP);
    gpio_init(E13,GPI,GPIO_HIGH,GPI_PULL_UP);
    gpio_init(E14,GPI,GPIO_HIGH,GPI_PULL_UP);
}

u8 KEY_Scan(u8 mode)
{
    static u8 key_up=1;//按键按松开标志

    if(mode)key_up=1;  //支持连按
    if(key_up&&(KEY3==0||KEY4==0||KEY5==0))
    {
        systick_delay_us(50000);
        key_up=0;
        if(KEY3==0)return KEY0_PRES;
        else if(KEY4==0)return KEY1_PRES;
        else if(KEY5==0)return KEY2_PRES;
    } else if(KEY3==1&&KEY4==1&&KEY5==1)key_up=1;
    return 0;// 无按键按下
}
