#ifndef __KEY_H
#define __KEY_H

#include "headfile.h"


#define KEY0_PRES 	1	//KEY0按下
#define KEY1_PRES	  2	//KEY1按
#define KEY2_PRES   3	//KEY2按下

#define KEY3 gpio_get(E12)//右
#define KEY4 gpio_get(E11)//按下
#define KEY5 gpio_get(E14)//左
void KEY_Init(void);
u8 KEY_Scan(u8 mode);

#endif