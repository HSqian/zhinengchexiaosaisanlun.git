#ifndef __MATH_H
#define __MATH_H

#include "headfile.h"

float err_get(float adc_L,float adc_LM,float adc_RM,float adc_R,float A,float B);

#endif