#include "motor.h"


void Motor_PWM_Init(void)
{
    mypwm_init(TIM_5,TIM_5_CH1_A00,0,6400,0);//左轮
    gpio_init( A1, GPO, 0, GPO_PUSH_PULL);
    mypwm_init(TIM_5,TIM_5_CH3_A02,0,6400,0);//右轮
    gpio_init( A3, GPO, 1, GPO_PUSH_PULL);
}

//限幅函数
void Limit(int *motorA,int *motorB)
{
    if( *motorA>PWM_MAX)*motorA=PWM_MAX;
    if( *motorA<PWM_MIN)*motorA=PWM_MIN;

    if( *motorB>PWM_MAX)*motorB=PWM_MAX;
    if( *motorB<PWM_MIN)*motorB=PWM_MIN;
}


//绝对值函数
int GFP_abs(int p)
{
    int q;
    q=p>0?p:(-p);
    return q;
}


//赋值函数
/*pid完成运算后的pwm值*/
void Load(int moto1,int moto2)
{
    int static i=0;
    if(stop_state==1||fork_state==4)
    {
        i++;
        if(i>500)
        {
            moto1=0;
            moto2=0;
        };
    }

    if (moto1>0) Linh;
    else 				 Linl;
    mypwm_duty_updata(TIM_5,TIM_5_CH1_A00,GFP_abs(moto1));

    if (moto2>0) Rinh;
    else 				 Rinl;
    mypwm_duty_updata(TIM_5,TIM_5_CH3_A02,GFP_abs(moto2));
}