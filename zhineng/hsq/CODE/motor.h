#ifndef __MOTOR_H
#define __MOTOR_H

#include "headfile.h"
//h 为正转，l 为反转
#define Rinh gpio_init( A3, GPO, 1, GPO_PUSH_PULL)	//右电机
#define Rinl gpio_init( A3, GPO, 0,GPO_PUSH_PULL)

#define Linh gpio_init( A1, GPO, 0, GPO_PUSH_PULL)//左电机
#define Linl gpio_init( A1, GPO, 1, GPO_PUSH_PULL)


void Motor_PWM_Init(void);
void Limit(int *motorA,int *motorB);
int GFP_abs(int p);
void Load(int moto1,int moto2);

#endif