/**************************************************************************
 文件名称：修改自逐飞代码，简洁优化了一部分
 平台信息：MDK5 ARM
 项目名称：
 作       者：DRZZZ
 当前版本：V1.0
 生成日期：
 最近修改：
**************************************************************************/
#include "pwm.h"


//-------------------------------------------------------------------------------------------------------------------
// @brief		PWM 引脚初始化 内部调用
// @param		pin				选择 PWM 引脚
// @return		void			NULL
// Sample usage:				pwm_pin_init(pin);
//-------------------------------------------------------------------------------------------------------------------
static void mypwm_pin_init (TIM_PWMPIN_enum pin)
{
    afio_init((PIN_enum)(pin &0xff), GPO, (GPIOAF_enum)((pin &0xf00)>>8), GPO_AF_PUSH_PUL);		// 提取对应IO索引 AF功能编码
}

//-------------------------------------------------------------------------------------------------------------------
// @brief		PWM 初始化（内部高速时钟，时钟频率为96000000）
// @param		tim				选择 PWM 使用的 TIM
// @param		pin				选择 PWM 引脚
// @param		psc				时钟预分频系数的设置
// @param		arr				自动重装载值的设置
// @param		duty			比较系数的设置
// @return		void
// Sample usage:						pwm_init(TIM_1, TIM_1_CH1_A08, 10000, 50000/100*ch1);
//-------------------------------------------------------------------------------------------------------------------
void mypwm_init (TIM_enum tim, TIM_PWMPIN_enum pin, uint32 psc,uint32 arr, uint32 duty)
{   // 占空比写入错误
    mypwm_pin_init(pin);																			// 初始化引脚
    if(tim & 0xf000)
        RCC->APB2ENR |= ((uint32_t)0x00000001 << ((tim&0x0ff0) >> 4));							// 使能时钟
    else
        RCC->APB1ENR |= ((uint32_t)0x00000001 << ((tim&0x0ff0) >> 4));							// 使能时钟

    tim_index[(tim&0x0f)]->ARR = arr;													// 装载自动重装载值
    tim_index[(tim&0x0f)]->PSC = psc;														// 装载预分频
    tim_index[(tim&0x0f)]->CR1 = TIM_CR1_ARPEN;													// 允许自动重装载值的预装载
    tim_index[(tim&0x0f)]->BDTR = TIM_BDTR_MOEN;												// PWM 输出使能

    switch(pin&0xf000)
    {
    case 0x1000:
        tim_index[(tim&0x0f)]->CCMR1 |=														// OC1M [6:4] 110
            TIM_CCMR1_IC1F_1 | TIM_CCMR1_IC1F_2;										// PWM 模式 1
        tim_index[(tim&0x0f)]->CCMR1 |= TIM_CCMR1_OC1PEN;									// 允许输出比较值的预装载
        tim_index[(tim&0x0f)]->CCER |= TIM_CCER_CC1EN;										// 使能通道 1
        tim_index[(tim&0x0f)]->CCR1 = duty;											// 装载比较值
        break;
    case 0x2000:
        tim_index[(tim&0x0f)]->CCMR1 |=														// OC1M [6:4] 110
            TIM_CCMR1_IC2F_1 | TIM_CCMR1_IC2F_2;										// PWM 模式 1
        tim_index[(tim&0x0f)]->CCMR1 |= TIM_CCMR1_OC2PEN;									// 允许输出比较值的预装载
        tim_index[(tim&0x0f)]->CCER |= TIM_CCER_CC2EN;										// 使能通道 2
        tim_index[(tim&0x0f)]->CCR2 = duty;											// 装载比较值
        break;
    case 0x3000:
        tim_index[(tim&0x0f)]->CCMR2 |=														// OC1M [6:4] 110
            TIM_CCMR2_IC3F_1 | TIM_CCMR2_IC3F_2;										// PWM 模式 1
        tim_index[(tim&0x0f)]->CCMR2 |= TIM_CCMR2_OC3PEN;									// 允许输出比较值的预装载
        tim_index[(tim&0x0f)]->CCER |= TIM_CCER_CC3EN;										// 使能通道 2
        tim_index[(tim&0x0f)]->CCR3 = duty;											// 装载比较值
        break;
    case 0x4000:
        tim_index[(tim&0x0f)]->CCMR2 |=														// OC1M [6:4] 110
            TIM_CCMR2_IC4F_1 | TIM_CCMR2_IC4F_2;										// PWM 模式 0
        tim_index[(tim&0x0f)]->CCMR2 |= TIM_CCMR2_OC4PEN;									// 允许输出比较值的预装载
        tim_index[(tim&0x0f)]->CCER |= TIM_CCER_CC4EN;										// 使能通道 2
        tim_index[(tim&0x0f)]->CCR4 = duty;											// 装载比较值
        break;
    }
    tim_index[(tim&0x0f)]->CR1 |= TIM_CR1_CEN;													// 使能定时器
}

//-------------------------------------------------------------------------------------------------------------------
// @brief		PWM 使能
// @param		tim				选择 PWM 使用的 TIM
// @return		void
// Sample usage:				pwm_enable(TIM_1);
//-------------------------------------------------------------------------------------------------------------------
void mypwm_enable (TIM_enum tim)
{
    tim_index[(tim&0x0f)]->CR1 |= TIM_CR1_CEN;													// 使能定时器
}

//-------------------------------------------------------------------------------------------------------------------
// @brief		PWM 失能
// @param		tim				选择 PWM 使用的 TIM
// @return		void
// Sample usage:				pwm_disable(TIM_1);
//-------------------------------------------------------------------------------------------------------------------
void mypwm_disable (TIM_enum tim)
{
    tim_index[(tim&0x0f)]->CR1 &= ~TIM_CR1_CEN;													// 失能定时器
}

//-------------------------------------------------------------------------------------------------------------------
// @brief		PWM 更新占空比
// @param		tim				选择 PWM 使用的 TIM
// @param		pin				选择 PWM 引脚
// @param		duty			设置占空比
// @return		void
// Sample usage:				pwm_duty_updata(TIM_1, TIM_1_CH1_A08, 500*duty);
//-------------------------------------------------------------------------------------------------------------------
void mypwm_duty_updata (TIM_enum tim, TIM_PWMPIN_enum pin, uint32 duty)
{
    switch(pin&0xf000)																			// 提取通道值
    {
    case 0x1000:
        tim_index[(tim&0x0f)]->CCR1 = duty;											// 装载比较值
        break;
    case 0x2000:
        tim_index[(tim&0x0f)]->CCR2 = duty;											// 装载比较值
        break;
    case 0x3000:
        tim_index[(tim&0x0f)]->CCR3 = duty;											// 装载比较值
        break;
    case 0x4000:
        tim_index[(tim&0x0f)]->CCR4 = duty;											// 装载比较值
        break;
    }
}



void Motor_Init(void)//未使用。使用Motor_PWM_Init在motor.c中。
{
    mypwm_init(TIM_5,TIM_5_CH1_A00, 0, 6400,0); // 初始化TIM2 频率15KHz 左
    mypwm_init(TIM_5,TIM_5_CH3_A02, 0, 6400,0); // 初始化TIM2 频率15KHz 右
    // 初始化引脚为推挽输出 默认高电平     方向管脚  高电平时正转，低电平时反转
    // A1 左轮  B3 右轮
    gpio_init( A1, GPO, 1, GPO_PUSH_PULL);
    gpio_init( A3, GPO, 1, GPO_PUSH_PULL);

}

