#include "scheduler.h"

int a;

volatile uint64_t systime_ms = 0;
int beep=0;
int wave_get=0;


/********************************************************************************
* Routine: Loop_1000Hz
* Description:
* Param: NULL
* Return: NULL
* Notes:
**********************************************************************************/
static void Loop_1000Hz(void)
{

}

/********************************************************************************
* Routine: Loop_500Hz
* Description:
* Param: NULL
* Return: NULL
* Notes:
**********************************************************************************/
static void Loop_500Hz(void)
{
    Encoder_Left  =  Encoder_Read(4);                                    //===读取编码器的值!!!为了保证M法测速的时间基准，首先读取编码器数据
    Encoder_Right =  -Encoder_Read(3);
    get_gyro();
    Get_RC();
    Kinematic_Analysis(Velocity,Turn);     															 //===小车运动学分析
    Motor_A=Incremental_PI_A(Encoder_Left, Target_A);                    //===速度闭环控制计算电机A最终PWM
    Motor_B=Incremental_PI_B(Encoder_Right,Target_B);                    //===速度闭环控制计算电机B最终PWM
    Limit(&Motor_A,&Motor_B);                                            //===PWM限幅
    Load(Motor_A,Motor_B);
//    gpio_toggle(B11);

}

/********************************************************************************
* Routine: Loop_333Hz
* Description:
* Param: NULL
* Return: NULL
* Notes:
**********************************************************************************/
static void Loop_333Hz(void)

{

}

/********************************************************************************
* Routine: Loop_200Hz
* Description:
* Param: NULL
* Return: NULL
* Notes:
**********************************************************************************/

static void Loop_200Hz(void)
{
    get_accdata();
    MahonyAHRSupdateIMU(mpu_gyro_x/2000.0f, mpu_gyro_y/2000.0f, mpu_gyro_z/2000.0f, mpu_acc_x, mpu_acc_y, mpu_acc_z, &Pitch, &Roll, &Yaw);
}

/********************************************************************************
* Routine: Loop_100Hz
* Description:
* Param: NULL
* Return: NULL
* Notes:
**********************************************************************************/
static void Loop_100Hz(void)
{

}

/********************************************************************************
* Routine: Loop_50Hz
* Description:
* Param: NULL
* Return: NULL
* Notes:
**********************************************************************************/
static void Loop_50Hz(void)
{
    Meeting_Hande();
}

/********************************************************************************
* Routine: Loop_20Hz
* Description:
* Param: NULL
* Return: NULL
* Notes:
**********************************************************************************/
static void Loop_20Hz(void)
{

}

/********************************************************************************
* Routine: Loop_5Hz
* Description:
* Param: NULL
* Return: NULL
* Notes:
**********************************************************************************/
static void Loop_5Hz(void)
{

}

/********************************************************************************
* Routine: Loop_2Hz
* Description:
* Param: NULL
* Return: NULL
* Notes:
**********************************************************************************/
static void Loop_2Hz(void)
{

}

/********************************************************************************
* Routine: Loop_1Hz
* Description:
* Param: NULL
* Return: NULL
* Notes:
**********************************************************************************/
static void Loop_1Hz(void)
{

}

//系统任务配置，创建不同执行频率的“线程”
static sched_task_t sched_tasks[] =
{
    {Loop_1000Hz,1000,  0, 0, 0},
    {Loop_500Hz , 500,  0, 0, 0},
    {Loop_333Hz , 333,  0, 0, 0},
    {Loop_200Hz , 200,  0, 0, 0},
    {Loop_100Hz , 100,  0, 0, 0},
    {Loop_50Hz  ,  50,  0, 0, 0},
    {Loop_20Hz  ,  20,  0, 0, 0},
    {Loop_5Hz   ,   5,  0, 0, 0},
    {Loop_2Hz   ,   2,  0, 0, 0},
    {Loop_1Hz   ,   1,  0, 0, 0},
};
//根据数组长度，判断线程数量
#define TASK_NUM (sizeof(sched_tasks)/sizeof(sched_task_t))

/********************************************************************************
* Routine: Scheduler_Init
* Description:
* Param: void
* Return: void
* Notes:
**********************************************************************************/
void Scheduler_Init(void)
{
    uint8_t index = 0;
    //初始化任务表
    for(index=0; index < TASK_NUM; index++)
    {
        //计算每个任务的延时周期数
        sched_tasks[index].interval_ticks = (uint16_t)(1000.0f/sched_tasks[index].rate_hz);
        //最短周期为1
        if(sched_tasks[index].interval_ticks < 1)
        {
            sched_tasks[index].interval_ticks = 1;
        }
    }
}

/********************************************************************************
* Routine: Scheduler_Run
* Description:
* Param: NULL
* Return: NULL
* Notes:
**********************************************************************************/
void Scheduler_Run(void)
{
    systime_ms++;
    uint8_t index = 0;
    uint32_t count = 0;
    //循环判断所有线程，是否应该执行

    for(index=0; index < TASK_NUM; index++)
    {
        count = TIM_GetCounter(TIM7);
        //进行判断，如果当前时间减去上一次执行的时间，大于等于该线程的执行周期，则执行线程
        //同时判断，如果本次1ms执行周期内有足够时间执行任务，则执行，否则存入下周期执行&& 1000-count >=sched_tasks[index].run_time
        if(systime_ms - sched_tasks[index].last_run >= sched_tasks[index].interval_ticks )
        {
            //更新线程的执行时间，用于下一次判断
            sched_tasks[index].last_run = systime_ms;
            //执行线程函数，使用的是函数指针
            sched_tasks[index].task_func();
            //记录线程执行时长
            sched_tasks[index].run_time = TIM_GetCounter(TIM7) - count;
        }
    }
}

/********************************************************************************
* Routine: GetSysTimeMs
* Description:
* Param: void
* Return: uint64_t
* Notes:
**********************************************************************************/
uint64_t GetSysTimeMs(void)
{
    return systime_ms;
}




