#ifndef _SCHEDULER_H
#define _SCHEDULER_H


#include "headfile.h"

typedef struct
{
    void(*task_func)(void);
    float rate_hz;
    uint16_t interval_ticks;
    uint32_t last_run;
    uint32_t run_time;
} sched_task_t;

void Scheduler_Init(void);
void Scheduler_Run(void);
uint64_t GetSysTimeMs(void);
uint64_t GetSysTimeUs(void);


#endif
