#include "tim.h"
#include "common.h"

void my_timer(void)
{
    TIM_TimeBaseInitTypeDef TIM_TypeStr;

    RCC_APB1PeriphClockCmd(RCC_APB1ENR_TIM7, ENABLE);

    TIM_TypeStr.TIM_ClockDivision=TIM_CKD_DIV1;
    TIM_TypeStr.TIM_CounterMode=TIM_CounterMode_Up;
    TIM_TypeStr.TIM_Period = 10-1;
    TIM_TypeStr.TIM_Prescaler =12000-1;
    TIM_TimeBaseInit(TIM7,&TIM_TypeStr);

    TIM_ITConfig(TIM7,TIM_IT_Update|TIM_IT_Trigger,ENABLE);

    nvic_init(TIM7_IRQn,1,0,ENABLE);

    TIM_Cmd(TIM7,ENABLE);
}



