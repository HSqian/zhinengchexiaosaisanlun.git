#include "ui.h"

Key_index_struct const Key_table[40]=
{
    //当前, 上一个，下一个, 确定
    {0,  23,  1,  4,(*menu1)},//一级界面第一行
    {1,   0,  2,  8,(*menu2)},//一级界面第二行
    {2,   1,  3, 12,(*menu3)},//一级界面第三行
    {3,   2, 20, 16,(*menu4)},//一级界面第四行
    {4,   7,  5,  4,(*subMenu1_1)},//1二级界面第一行
    {5,   4,  6,  5,(*subMenu1_2)},//1二级界面第二行
    {6,   5,  7,  6,(*subMenu1_3)},//1二级界面第三行
    {7,   6,  4,  0,(*subMenu1_4)},//1二级界面第四行
    {8,  11,  9,  8,(*subMenu2_1)},//2二级界面第一行
    {9,   8, 10,  9,(*subMenu2_2)},//2二级界面第二行
    {10,  9, 11, 10,(*subMenu2_3)},//2二级界面第三行
    {11, 10,  8,  1,(*subMenu2_4)},//2二级界面第四行
    {12, 15, 13, 12,(*subMenu3_1)},//3二级界面第一行
    {13, 12, 14, 13,(*subMenu3_2)},//3二级界面第二行
    {14, 13, 15, 14,(*subMenu3_3)},//3二级界面第三行
    {15, 14, 12,  2,(*subMenu3_4)},//3二级界面第四行
    {16, 19, 17, 16,(*subMenu4_1)},//4二级界面第一行
    {17, 16, 18, 17,(*subMenu4_2)},//4二级界面第二行
    {18, 15, 19, 18,(*subMenu4_3)},//4二级界面第三行
    {19, 18, 16,  3,(*subMenu4_4)},//4二级界面第四行
    {20,  3, 21, 24,(*menu5)},//一级五
    {21, 20, 22, 28,(*menu6)},//一级六
    {22, 21, 23, 32,(*menu7)},//一级七
    {23, 22,  0, 36,(*menu8)},//一级八
    {24, 27, 25, 24,(*subMenu5_1)},//五一
    {25, 24, 26, 25,(*subMenu5_2)},//五二
    {26, 25, 27, 26,(*subMenu5_3)},//53
    {27, 26, 24, 20,(*subMenu5_4)},//54
    {28, 31, 29, 28,(*subMenu6_1)},//61
    {29, 28, 30, 29,(*subMenu6_2)},//62
    {30, 29, 31, 30,(*subMenu6_3)},//63
    {31, 30, 28, 21,(*subMenu6_4)},//64
    {32, 35, 33, 32,(*subMenu7_1)},//71
    {33, 32, 34, 33,(*subMenu7_2)},//72
    {34, 33, 35, 34,(*subMenu7_3)},//73
    {35, 34, 32, 22,(*subMenu7_4)},//74
    {36, 39, 37, 36,(*subMenu8_1)},//81
    {37, 36, 38, 37,(*subMenu8_2)},//82
    {38, 37, 39, 38,(*subMenu8_3)},//83
    {39, 38, 36, 23,(*subMenu8_4)},//84
};

u8 nowIndex = 0;
float static v_kp=0,v_ki=0,vel=0,t_kp=0,t_kd=0,e_kp=0,gyro_kp=0,real_kp=0,real_kd=0,v_ring=0,v_forkin=0,v_forkget=0;//pid调试的值
u32 pid[12]= {0,0,0,0,0,0,0,0,0,0,0,0}; //pid储存
static int clc=1;


void LCD_display(void)
{

    u8 key=0;

    key=KEY_Scan(0);
    flash_page_read(FLASH_SECTION_15,FLASH_PAGE_0,pid,32);
    switch(key)
    {
    case KEY2_PRES:
        lcd_clear(BLACK);
        nowIndex=Key_table[nowIndex].up_index;
        break;
    case KEY0_PRES:
        lcd_clear(BLACK);
        nowIndex=Key_table[nowIndex].next_index;
        break;
    case KEY1_PRES:
        lcd_clear(BLACK);
        nowIndex=Key_table[nowIndex].enter_index;
        break;
    }

    Key_table[nowIndex].operate();
}

void menu1(void)//菜单列表
{
    lcd_showstr(20,0,"VELOCITY");
    lcd_showstr(20,2,"TURN");
    lcd_showstr(20,4,"GYRO");
    lcd_showstr(20,6,"menu4");
    Draw_Circle(10,8,5,WHITE);
}
void menu2(void)//菜单列表
{
    lcd_showstr(20,0,"VELOCITY");
    lcd_showstr(20,2,"TURN");
    lcd_showstr(20,4,"GYRO");
    lcd_showstr(20,6,"menu4");
    Draw_Circle(10,40,5,WHITE);
}

void menu3(void)//菜单列表
{
    lcd_showstr(20,0,"VELOCITY");
    lcd_showstr(20,2,"TURN");
    lcd_showstr(20,4,"GYRO");
    lcd_showstr(20,6,"menu4");
    Draw_Circle(10,72,5,WHITE);
}
void menu4(void)//菜单列表
{
    lcd_showstr(20,0,"VELOCITY");
    lcd_showstr(20,2,"TURN");
    lcd_showstr(20,4,"GYRO");
    lcd_showstr(20,6,"menu4");
    Draw_Circle(10,104,5,WHITE);
}
void menu5(void)//菜单列表
{
    lcd_showstr(20,0,"menu5");
    lcd_showstr(20,2,"menu6");
    lcd_showstr(20,4,"menu7");
    lcd_showstr(20,6,"menu8");
    Draw_Circle(10,8,5,WHITE);
}
void menu6(void)//菜单列表
{
    lcd_showstr(20,0,"menu5");
    lcd_showstr(20,2,"menu6");
    lcd_showstr(20,4,"menu7");
    lcd_showstr(20,6,"menu8");
    Draw_Circle(10,40,5,WHITE);
}
void menu7(void)//菜单列表
{
    lcd_showstr(20,0,"menu5");
    lcd_showstr(20,2,"menu6");
    lcd_showstr(20,4,"menu7");
    lcd_showstr(20,6,"menu8");
    Draw_Circle(10,72,5,WHITE);
}
void menu8(void)//菜单列表
{
    lcd_showstr(20,0,"menu5");
    lcd_showstr(20,2,"menu6");
    lcd_showstr(20,4,"menu7");
    lcd_showstr(20,6,"menu8");
    Draw_Circle(10,104,5,WHITE);

}

//=======================子菜单1==========================
void subMenu1_1(void)//子菜单列表
{
    lcd_clear(BLACK);
    v_kp=pid[0];
    v_ki=pid[1];
    vel=pid[2];
    lcd_debug1(&v_kp,10,8,1);
}

void subMenu1_2(void)//子菜单列表
{
    lcd_clear(BLACK);
    v_kp=pid[0];
    v_ki=pid[1];
    vel=pid[2];
    lcd_debug1(&v_ki,10,40,1);
}

void subMenu1_3(void)//子菜单列表
{
    lcd_clear(BLACK);
    v_kp=pid[0];
    v_ki=pid[1];
    vel=pid[2];
    lcd_debug1(&vel,10,72,1);
}

void subMenu1_4(void)//子菜单列表
{
    Draw_Circle(10,104,5,WHITE);
    lcd_showstr(20,0,"v_kp");
    lcd_showstr(20,2,"v_ki");
    lcd_showstr(20,4,"Vel");
    lcd_showstr(20,6,"exti");
    lcd_showfloat(60,0,v_kp,3,3);
    lcd_showfloat(60,2,v_ki/10000,3,4);
    lcd_showfloat(60,4,vel,3,3);
    v_kp=pid[0];
    v_ki=pid[1];
    vel=pid[2];
}
//======================子菜单2===========================
void subMenu2_1(void)//子菜单列表
{
    lcd_clear(BLACK);
    t_kp=pid[3];
    t_kd=pid[4];
    e_kp=pid[5];
    lcd_debug2(&t_kp,10,8,10);
}

void subMenu2_2(void)//子菜单列表
{
    lcd_clear(BLACK);
    t_kp=pid[3];
    t_kd=pid[4];
    e_kp=pid[5];
    lcd_debug2(&t_kd,10,40,1);

}

void subMenu2_3(void)//子菜单列表
{
    lcd_clear(BLACK);
    t_kp=pid[3];
    t_kd=pid[4];
    e_kp=pid[5];
    lcd_debug2(&e_kp,10,72,1);
}

void subMenu2_4(void)//子菜单列表
{
    Draw_Circle(10,104,5,WHITE);
    lcd_showstr(20,0,"t_kp");
    lcd_showstr(20,2,"t_kd");
    lcd_showstr(20,4,"e_kp");
    lcd_showstr(20,6,"exti");
    lcd_showfloat(60,0,t_kp,4,3);
    lcd_showfloat(60,2,t_kd/1000,4,3);
    lcd_showfloat(60,4,e_kp/100,4,3);
    t_kp=pid[3];
    t_kd=pid[4];
    e_kp=pid[5];

}
//======================子菜单3===========================
void subMenu3_1(void)//子菜单列表
{
    lcd_clear(BLACK);
    gyro_kp=pid[6];
    real_kp=pid[7];
    real_kd=pid[8];
    lcd_debug3(&gyro_kp,10,8,1);
}

void subMenu3_2(void)//子菜单列表
{
    lcd_clear(BLACK);
    gyro_kp=pid[6];
    real_kp=pid[7];
    real_kd=pid[8];
    lcd_debug3(&real_kp,10,40,1);
}

void subMenu3_3(void)//子菜单列表
{
    lcd_clear(BLACK);
    gyro_kp=pid[6];
    real_kp=pid[7];
    real_kd=pid[8];
    lcd_debug3(&real_kd,10,72,1);
}

void subMenu3_4(void)//子菜单列表
{
    Draw_Circle(10,104,5,WHITE);
    lcd_showstr(20,0,"g_kp");
    lcd_showstr(20,2,"r_kp");
    lcd_showstr(20,6,"r_kd");
    lcd_showstr(20,6,"exit");
    lcd_showfloat(60,0,gyro_kp/10,4,3);
    lcd_showfloat(60,2,real_kp/10,4,3);
    lcd_showfloat(60,4,real_kd/100,4,3);
    gyro_kp=pid[6];
    real_kp=pid[7];
    real_kd=pid[8];
}

void subMenu4_1(void)//子菜单列表
{
    lcd_showstr(20,0,"watch");
    lcd_showstr(20,2,"adc_org");
    lcd_showstr(20,4,"START");
    lcd_showstr(20,6,"exit");
    lcd_watch1(10,8);
}

void subMenu4_2(void)//子菜单列表
{
    lcd_showstr(20,0,"watch");
    lcd_showstr(20,2,"adc_org");
    lcd_showstr(20,4,"START");
    lcd_showstr(20,6,"exit");
    lcd_watch2(10,40);
}

void subMenu4_3(void)//子菜单列表
{
    char flag=1;
    vu8 key=0;

    lcd_showstr(20,0,"watch");
    lcd_showstr(20,2,"adc_org");
    lcd_showstr(20,4,"START");
    lcd_showstr(20,6,"exit");
    Draw_Circle(10,72,5,WHITE);

    while(flag)
    {
        key=KEY_Scan(1);
        switch(key)
        {
        case KEY0_PRES:
            flag=0;
            nowIndex=Key_table[nowIndex].next_index;
            systick_delay_ms(100);
            break;
        case KEY2_PRES:
            flag=0;
            nowIndex=Key_table[nowIndex].up_index;
            systick_delay_ms(100);
            break;
        case KEY1_PRES:
            flag=0;
            flag_lcd=0;
            break;
        }
        v_kp=pid[0];
        v_ki=pid[1];
        vel=pid[2];
        t_kp=pid[3];
        t_kd=pid[4];
        e_kp=pid[5];
        gyro_kp=pid[6];
        real_kp=pid[7];
        real_kd=pid[8];
        v_ring=pid[9];
        v_forkin=pid[10];
        v_forkget=pid[11];

        Velocity_KP=v_kp;
        Velocity_KI=v_ki/10000;

        Turn_KP=t_kp;
        Turn_KD=t_kd/1000;
        exp_turn_KP=e_kp/100;
        Velocity=vel;
        Velocity_MAX=vel;

        Gyro_kp=gyro_kp/10;
        Real_kp=real_kp/10;
        Real_kd=real_kd/100;

        V_ring=v_ring;
        V_forkin=v_forkin;
        V_forkget=v_forkget;
    }
    lcd_clear(BLACK);
}

void subMenu4_4(void)//子菜单列表
{
    lcd_showstr(20,0,"watch");
    lcd_showstr(20,2,"adc_org");
    lcd_showstr(20,4,"START");
    lcd_showstr(20,6,"exit");
    Draw_Circle(10,104,5,WHITE);
}

//======================子菜单5============================
void subMenu5_1(void)//子菜单列表
{
    lcd_clear(BLACK);
    v_ring=pid[9];
    v_forkin=pid[10];
    v_forkget=pid[11];
    lcd_debug4(&v_ring,10,8,5);
}

void subMenu5_2(void)//子菜单列表
{
    lcd_clear(BLACK);
    v_ring=pid[9];
    v_forkin=pid[10];
    v_forkget=pid[11];
    lcd_debug4(&v_forkin,10,40,5);
}

void subMenu5_3(void)//子菜单列表
{
    lcd_clear(BLACK);
    v_ring=pid[9];
    v_forkin=pid[10];
    v_forkget=pid[11];
    lcd_debug4(&v_forkget,10,72,5);
}

void subMenu5_4(void)//子菜单列表
{
    lcd_showstr(20,0,"v_ring");
    lcd_showstr(20,2,"v_forkin");
    lcd_showstr(20,4,"v_forkget");
    lcd_showstr(20,6,"exit");
    Draw_Circle(10,104,5,WHITE);
}

//======================子菜单5===========================
void subMenu6_1(void)//子菜单列表
{
    lcd_showstr(20,0,"subMenu6_1");
    lcd_showstr(20,2,"subMenu6_2");
    lcd_showstr(20,4,"subMenu6_3");
    lcd_showstr(20,6,"exit");
    Draw_Circle(10,8,5,WHITE);
}

void subMenu6_2(void)//子菜单列表
{
    lcd_showstr(20,0,"subMenu6_1");
    lcd_showstr(20,2,"subMenu6_2");
    lcd_showstr(20,4,"subMenu6_3");
    lcd_showstr(20,6,"exit");
    Draw_Circle(10,40,5,WHITE);
}

void subMenu6_3(void)//子菜单列表
{
    lcd_showstr(20,0,"subMenu6_1");
    lcd_showstr(20,2,"subMenu6_2");
    lcd_showstr(20,4,"subMenu6_3");
    lcd_showstr(20,6,"exit");
    Draw_Circle(10,72,5,WHITE);
}

void subMenu6_4(void)//子菜单列表
{
    lcd_showstr(20,0,"subMenu6_1");
    lcd_showstr(20,2,"subMenu6_2");
    lcd_showstr(20,4,"subMenu6_3");
    lcd_showstr(20,6,"exit");
    Draw_Circle(10,104,5,WHITE);
}
//======================子菜单7===========================
void subMenu7_1(void)//子菜单列表
{
    lcd_showstr(20,0,"subMenu7_1");
    lcd_showstr(20,2,"subMenu7_2");
    lcd_showstr(20,4,"subMenu7_3");
    lcd_showstr(20,6,"exit");
    Draw_Circle(10,8,5,WHITE);
}

void subMenu7_2(void)//子菜单列表
{
    lcd_showstr(20,0,"subMenu7_1");
    lcd_showstr(20,2,"subMenu7_2");
    lcd_showstr(20,4,"subMenu7_3");
    lcd_showstr(20,6,"exit");
    Draw_Circle(10,40,5,WHITE);
}

void subMenu7_3(void)//子菜单列表
{
    lcd_showstr(20,0,"subMenu7_1");
    lcd_showstr(20,2,"subMenu7_2");
    lcd_showstr(20,4,"subMenu7_3");
    lcd_showstr(20,6,"exit");
    Draw_Circle(10,72,5,WHITE);
}

void subMenu7_4(void)//子菜单列表
{
    lcd_showstr(20,0,"subMenu7_1");
    lcd_showstr(20,2,"subMenu7_2");
    lcd_showstr(20,4,"subMenu7_3");
    lcd_showstr(20,6,"exit");
    Draw_Circle(10,104,5,WHITE);
}
//======================子菜单8===========================
void subMenu8_1(void)//子菜单列表
{
    lcd_showstr(20,0,"subMenu8_1");
    lcd_showstr(20,2,"subMenu8_2");
    lcd_showstr(20,4,"subMenu8_3");
    lcd_showstr(20,6,"exit");
    Draw_Circle(10,8,5,WHITE);
}

void subMenu8_2(void)//子菜单列表
{
    lcd_showstr(20,0,"subMenu8_1");
    lcd_showstr(20,2,"subMenu8_2");
    lcd_showstr(20,4,"subMenu8_3");
    lcd_showstr(20,6,"exit");
    Draw_Circle(10,40,5,WHITE);
}

void subMenu8_3(void)//子菜单列表
{
    lcd_showstr(20,0,"subMenu8_1");
    lcd_showstr(20,2,"subMenu8_2");
    lcd_showstr(20,4,"subMenu8_3");
    lcd_showstr(20,6,"exit");
    Draw_Circle(10,72,5,WHITE);
}

void subMenu8_4(void)//子菜单列表
{
    lcd_showstr(20,0,"subMenu8_1");
    lcd_showstr(20,2,"subMenu8_2");
    lcd_showstr(20,4,"subMenu8_3");
    lcd_showstr(20,6,"exit");
    Draw_Circle(10,104,5,WHITE);
}

void ring(int x,int y)
{
    Draw_Circle(x,y,5,WHITE);
    systick_delay_ms(5);
    Draw_Circle(x,y,5,BLACK);
    systick_delay_ms(5);
}
void lcd_debug1(float *kx,int x ,int y ,float add)
{
    int fir=1,sec=0,key;

    lcd_showstr(20,0,"v_kp");
    lcd_showstr(20,2,"v_ki");
    lcd_showstr(20,4,"Vel");
    lcd_showstr(20,6,"exti");
    lcd_showfloat(60,0,v_kp,4,3);
    lcd_showfloat(60,2,v_ki/10000,4,4);
    lcd_showfloat(60,4,vel,4,3);

    while(fir)
    {
        ring(x,y);
        key=KEY_Scan(0);
        switch(key)
        {
        case KEY0_PRES:
            fir=0;
            nowIndex=Key_table[nowIndex].next_index;
            systick_delay_ms(100);
            break;
        case KEY2_PRES:
            fir=0;
            nowIndex=Key_table[nowIndex].up_index;
            systick_delay_ms(100);
            break;
        case KEY1_PRES:
            sec=1;
            systick_delay_ms(100);
            break;
        }
        while(sec)
        {
            lcd_showfloat(60,0,v_kp,4,3);
            lcd_showfloat(60,2,v_ki/10000,4,4);
            lcd_showfloat(60,4,vel,4,3);
            Draw_Circle(x,y,5,WHITE);

            key=KEY_Scan(1);

            switch(key)
            {
            case KEY0_PRES:
                *kx=*kx-add;
                systick_delay_ms(10);
                break;
            case KEY2_PRES:
                *kx=*kx+add;
                systick_delay_ms(10);
                break;
            case KEY1_PRES:
                sec=0;
                fir=0;
                pid[0]=v_kp;
                pid[1]=v_ki;
                pid[2]=vel;
                Velocity_KP=v_kp;
                Velocity_KI=v_ki/10000;
                Velocity=vel;
                flash_page_program(FLASH_SECTION_15,FLASH_PAGE_0,pid,32);//储存pid的值
                systick_delay_ms(10);
                break;
            }

        }
    }
}

void lcd_debug2(float *kx,int x ,int y ,float add)
{
    int fir=1,sec=0,key;

    lcd_showstr(20,0,"t_kp");
    lcd_showstr(20,2,"t_kd");
    lcd_showstr(20,4,"e_kp");
    lcd_showstr(20,6,"exti");
    lcd_showfloat(60,0,t_kp,4,3);
    lcd_showfloat(60,2,t_kd/1000,4,3);
    lcd_showfloat(60,4,e_kp/100,4,3);
    //----------进入页面选择（上/下）
    while(fir)
    {
        ring(x,y);
        key=KEY_Scan(0);
        switch(key)
        {
        case KEY0_PRES:
            fir=0;
            nowIndex=Key_table[nowIndex].next_index;
            systick_delay_ms(100);
            break;
        case KEY2_PRES:
            fir=0;
            nowIndex=Key_table[nowIndex].up_index;
            systick_delay_ms(100);
            break;
        case KEY1_PRES:
            sec=1;
            systick_delay_ms(100);
            break;
        }
        //----------调参
        while(sec)
        {
            lcd_showfloat(60,0,t_kp,4,3);
            lcd_showfloat(60,2,t_kd/1000,4,3);
            lcd_showfloat(60,4,e_kp/100,4,3);
            Draw_Circle(x,y,5,WHITE);

            key=KEY_Scan(1);

            switch(key)
            {
            case KEY0_PRES:
                *kx=*kx-add;
                systick_delay_ms(10);
                break;
            case KEY2_PRES:
                *kx=*kx+add;
                systick_delay_ms(10);
                break;
            case KEY1_PRES:
                sec=0;
                fir=0;
                pid[3]=t_kp;
                pid[4]=t_kd;
                pid[5]=e_kp;
                Turn_KP=t_kp;
                Turn_KD=t_kd/1000;
                exp_turn_KP=e_kp/100;
                flash_page_program(FLASH_SECTION_15,FLASH_PAGE_0,pid,32);//储存pid的值
                systick_delay_ms(10);
                break;
            }
        }
    }
}

void lcd_debug3(float *kx,int x ,int y ,float add)
{
    int fir=1,sec=0,key;

    lcd_showstr(20,0,"g_kp");
    lcd_showstr(20,2,"r_kp");
    lcd_showstr(20,4,"r_kd");
    lcd_showstr(20,6,"exit");
    lcd_showfloat(60,0,gyro_kp/10,4,3);
    lcd_showfloat(60,2,real_kp/10,4,3);
    lcd_showfloat(60,4,real_kd/100,4,3);
    //----------进入页面选择（上/下）
    while(fir)
    {
        ring(x,y);
        key=KEY_Scan(0);
        switch(key)
        {
        case KEY0_PRES:
            fir=0;
            nowIndex=Key_table[nowIndex].next_index;
            systick_delay_ms(100);
            break;
        case KEY2_PRES:
            fir=0;
            nowIndex=Key_table[nowIndex].up_index;
            systick_delay_ms(100);
            break;
        case KEY1_PRES:
            sec=1;
            systick_delay_ms(100);
            break;
        }
        //----------调参
        while(sec)
        {
            lcd_showfloat(60,0,gyro_kp/10,4,3);
            lcd_showfloat(60,2,real_kp/10,4,3);
            lcd_showfloat(60,4,real_kd/100,4,3);
            Draw_Circle(x,y,5,WHITE);

            key=KEY_Scan(1);

            switch(key)
            {
            case KEY0_PRES:
                *kx=*kx-add;
                systick_delay_ms(10);
                break;
            case KEY2_PRES:
                *kx=*kx+add;
                systick_delay_ms(10);
                break;
            case KEY1_PRES:
                sec=0;
                fir=0;
                pid[6]=gyro_kp;
                pid[7]=real_kp;
                pid[8]=real_kd;
                Gyro_kp=gyro_kp/10;
                Real_kp=real_kp/10;
                Real_kd=real_kd/100;
                flash_page_program(FLASH_SECTION_15,FLASH_PAGE_0,pid,32);//储存pid的值
                systick_delay_ms(10);
                break;
            }
        }
    }
}

void lcd_debug4(float *kx,int x ,int y ,float add)
{
    int fir=1,sec=0,key;

    lcd_showstr(20,0,"v_ring");
    lcd_showstr(20,2,"v_forkin");
    lcd_showstr(20,4,"v_forkget");
    lcd_showstr(20,6,"exit");
    lcd_showfloat(120,0,v_ring,4,3);
    lcd_showfloat(120,2,v_forkin,4,3);
    lcd_showfloat(120,4,v_forkget,4,3);
    //----------进入页面选择（上/下）
    while(fir)
    {
        ring(x,y);
        key=KEY_Scan(0);
        switch(key)
        {
        case KEY0_PRES:
            fir=0;
            nowIndex=Key_table[nowIndex].next_index;
            systick_delay_ms(100);
            break;
        case KEY2_PRES:
            fir=0;
            nowIndex=Key_table[nowIndex].up_index;
            systick_delay_ms(100);
            break;
        case KEY1_PRES:
            sec=1;
            systick_delay_ms(100);
            break;
        }
        //----------调参
        while(sec)
        {
            lcd_showfloat(120,0,v_ring,4,3);
            lcd_showfloat(120,2,v_forkin,4,3);
            lcd_showfloat(120,4,v_forkget,4,3);
            Draw_Circle(x,y,5,WHITE);

            key=KEY_Scan(1);

            switch(key)
            {
            case KEY0_PRES:
                *kx=*kx-add;
                systick_delay_ms(10);
                break;
            case KEY2_PRES:
                *kx=*kx+add;
                systick_delay_ms(10);
                break;
            case KEY1_PRES:
                sec=0;
                fir=0;
                pid[9]=v_ring;
                pid[10]=v_forkin;
                pid[11]=v_forkget;
                V_ring=v_ring;
                V_forkin=v_forkin;
                V_forkget=v_forkget;
                flash_page_program(FLASH_SECTION_15,FLASH_PAGE_0,pid,32);//储存pid的值
                systick_delay_ms(10);
                break;
            }
        }
    }
}

void lcd_watch1(int x,int y)
{
    int fir=1,sec=0,key;

    while(fir)
    {
        ring(x,y);
        key=KEY_Scan(0);
        switch(key)
        {
        case KEY0_PRES:
            fir=0;
            nowIndex=Key_table[nowIndex].next_index;
            systick_delay_ms(100);
            break;
        case KEY2_PRES:
            fir=0;
            nowIndex=Key_table[nowIndex].up_index;
            systick_delay_ms(100);
            break;
        case KEY1_PRES:
            sec=1;
            systick_delay_ms(100);
            break;
        }
        clc=1;
        while(sec)
        {
            adc_watch();
            key=KEY_Scan(1);
            if(key!=0)
            {
                fir=0;
                sec=0;
                lcd_clear(BLACK);
            }
        }

    }

}

void lcd_watch2(int x,int y)
{
    int fir=1,sec=0,key;

    while(fir)
    {
        ring(x,y);
        key=KEY_Scan(0);
        switch(key)
        {
        case KEY0_PRES:
            fir=0;
            nowIndex=Key_table[nowIndex].next_index;
            systick_delay_ms(100);
            break;
        case KEY2_PRES:
            fir=0;
            nowIndex=Key_table[nowIndex].up_index;
            systick_delay_ms(100);
            break;
        case KEY1_PRES:
            sec=1;
            systick_delay_ms(100);
            break;
        }
        clc=1;
        while(sec)
        {
            org_adc();
            key=KEY_Scan(1);
            if(key!=0)
            {
                fir=0;
                sec=0;
                lcd_clear(BLACK);
            }
        }

    }

}
void adc_watch(void)
{
    if(clc)
    {
        lcd_clear(BLACK);
        clc=0;
    }
    Get_RC();
    Kinematic_Analysis(Velocity,Turn);

    systick_delay_ms(1);
    lcd_showstr(0,0,"RR");
    lcd_showstr(0,1,"R");
    lcd_showstr(0,2,"RM");
    lcd_showstr(0,3,"LM");
    lcd_showstr(0,4,"L");
    lcd_showstr(0,5,"LL");
    lcd_showstr(130,0,"ring");
    lcd_showstr(130,2,"fork");
    lcd_showstr(130,4,"stop");
    lcd_showstr(130,6,"ramp");
    lcd_showstr(0,7,"err");
    lcd_showstr(0,6,"A");
    lcd_showstr(50,6,"B");
    lcd_showfloat(120,1,ring_state,1,1);
    lcd_showfloat(120,3,fork_state,1,1);
    lcd_showfloat(120,5,stop_state,1,1);
    lcd_showfloat(120,7,ramp_state,1,1);
    lcd_showfloat(20,0,ADC_RR,3,3);
    lcd_showfloat(20,1,ADC_R,3,3);
    lcd_showfloat(20,2,ADC_RM,3,3);
    lcd_showfloat(20,3,ADC_LM,3,3);
    lcd_showfloat(20,4,ADC_L,3,3);
    lcd_showfloat(20,5,ADC_LL,3,3);
    lcd_showfloat(35,7,err,4,3);
    lcd_showint8(10,6,TURN_A);
    lcd_showint8(60,6,TURN_B);
}

void org_adc(void)
{
    if(clc)
    {
        lcd_clear(BLACK);
        clc=0;
    }
    lcd_showstr(0,0,"RR");
    lcd_showstr(0,1,"R");
    lcd_showstr(0,2,"RM");
    lcd_showstr(0,3,"LM");
    lcd_showstr(0,4,"L");
    lcd_showstr(0,5,"LL");

    ADC_RR =adc_mean_filter(ADC_2,ADC2_CH04_A04, 5);
    ADC_R=adc_mean_filter(ADC_2,ADC2_CH05_A05 ,5);
    ADC_RM  =adc_mean_filter(ADC_2,ADC2_CH06_A06	,5);
    ADC_LM=adc_mean_filter(ADC_2,ADC2_CH07_A07	,5);
    ADC_L =adc_mean_filter(ADC_2,ADC2_CH14_C04	,5);
    ADC_LL =adc_mean_filter(ADC_2,ADC2_CH15_C05	,5);
    lcd_showfloat(20,0,ADC_RR,5,3);
    lcd_showfloat(20,1,ADC_R,5,3);
    lcd_showfloat(20,2,ADC_RM,5,3);
    lcd_showfloat(20,3,ADC_LM,5,3);
    lcd_showfloat(20,4,ADC_L,5,3);
    lcd_showfloat(20,5,ADC_LL,5,3);
}