#ifndef __UI_H
#define __UI_H

#include "headfile.h"

typedef struct
{
    u8 current_index;	//存放当前界面的索引号；
    u8 up_index;
    u8 next_index;//存放按下“next”键需要跳转的索引号；
    u8 enter_index;		//存放按下“enter（进入）”键时需要跳转的索引号；
    void (*operate)();	//函数指针变量，存放当前索引号下需要执行的函数的入口地址。
} Key_index_struct;

void LCD_display(void);

void menu1(void);//菜单列表
void menu2(void);//菜单列表
void menu3(void);//菜单列表
void menu4(void);//菜单列表
void menu5(void);//菜单列表
void menu6(void);//菜单列表
void menu7(void);//菜单列表
void menu8(void);//菜单列表

void subMenu1_1(void);//子菜单
void subMenu1_2(void);//子菜单
void subMenu1_3(void);//子菜单
void subMenu1_4(void);//子菜单

void subMenu2_1(void);//子菜单
void subMenu2_2(void);//子菜单
void subMenu2_3(void);//子菜单
void subMenu2_4(void);//子菜单

void subMenu3_1(void);//子菜单
void subMenu3_2(void);//子菜单
void subMenu3_3(void);//子菜单
void subMenu3_4(void);//子菜单

void subMenu4_1(void);//子菜单
void subMenu4_2(void);//子菜单
void subMenu4_3(void);//子菜单
void subMenu4_4(void);//子菜单

void subMenu5_1(void);//子菜单
void subMenu5_2(void);//子菜单
void subMenu5_3(void);//子菜单
void subMenu5_4(void);//子菜单

void subMenu6_1(void);//子菜单
void subMenu6_2(void);//子菜单
void subMenu6_3(void);//子菜单
void subMenu6_4(void);//子菜单

void subMenu7_1(void);//子菜单
void subMenu7_2(void);//子菜单
void subMenu7_3(void);//子菜单
void subMenu7_4(void);//子菜单

void subMenu8_1(void);//子菜单
void subMenu8_2(void);//子菜单
void subMenu8_3(void);//子菜单
void subMenu8_4(void);//子菜单

extern u32 pid[12];
void ring(int x,int y);
void Lcd_dinum(int x,int y);
void lcd_debug1(float *kx,int x ,int y ,float add);
void lcd_debug2(float *kx,int x ,int y ,float add);
void lcd_debug3(float *kx,int x ,int y ,float add);
void lcd_debug4(float *kx,int x ,int y ,float add);
void lcd_watch1(int x,int y);
void lcd_watch2(int x,int y);
void adc_watch(void);
void org_adc(void);

#endif