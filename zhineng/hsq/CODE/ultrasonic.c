/******************************************************************************
* 以下所有内容版权均属戴润泽所有，未经允许不得用于商业用途
* @file
* @date
* @school    湖北工业大学
* @Software  MDK5 V5.32
* @author    hsq
* @version   V1.0
* @Target    CORE MM32SPIN2XPs
******************************************************************************/
#include "ultrasonic.h"

uint16 ranging_counter = 0;															// 测距数据
uint8 ranging_flage = 0x00;															// 测距状态
uint8 uart_data;
uint8 dat[3];
uint8 num;

void ultrasonic_init()
{
    gpio_init(SEND_PORT_PIN, GPO, GPIO_HIGH, GPO_PUSH_PULL);	// 初始化有去模块使能引脚
    uart_init(SPLIT_ULTRASONIC_UART, SPLIT_ULTRASONIC_BAUD, SPLIT_ULTRASONIC_TX, SPLIT_ULTRASONIC_RX);
    uart_rx_irq(SPLIT_ULTRASONIC_UART, 1);
}


void uart_handler (void)
{
    uart_query(SPLIT_ULTRASONIC_UART, &uart_data);
    dat[num] = uart_data;
    if(dat[0] != 0xa5)	num = 0;													//检查第一个数据是否为0xa5
    else				num++;

    if(num == 3)																	//接收完成 开始处理数据
    {
        num = 0;
        ranging_counter = dat[1]<<8 | dat[2];										//得到超声波模块测出的距离
        ranging_flage = 0x01;
    }
}


void Meeting_Hande()
{
    int static num=0;
    if(ranging_flage)		   // 等待测距完毕
    {
        if(ranging_counter<800)// 输出测距信息，小于2000则开启三岔检测
        {
            num++;
            if(num==4)
            {
                ranging_flage = 0x00;
                wave_get=1;
							num=0;
            }
        }
    }
}