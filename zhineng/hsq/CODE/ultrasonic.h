/******************************************************************************
* 以下所有内容版权均属戴润泽所有，未经允许不得用于商业用途
* @file
* @date
* @school    湖北工业大学
* @Software  MDK5 V5.32
* @author    戴润泽
* @version   V1.0
* @Target    CORE MM32SPIN2XPs
******************************************************************************/
#ifndef _ULTRASONIC_H
#define _ULTRASONIC_H

#include "headfile.h"

#define SPLIT_ULTRASONIC_UART		UART_3   		// 超声波串口号
#define SPLIT_ULTRASONIC_BAUD		115200   		// 超声波波特率
#define SPLIT_ULTRASONIC_TX			UART3_TX_B10	// 引脚重定义
#define SPLIT_ULTRASONIC_RX			UART3_RX_B11

#define SEND_PORT_PIN				B9			    // 设置对应端口 可有可无

extern uint16 ranging_counter;						// 测得的距离
extern uint8 ranging_flage;						    // 测距状态

void ultrasonic_init();
void Meeting_Hande();
void uart_handler();
#endif