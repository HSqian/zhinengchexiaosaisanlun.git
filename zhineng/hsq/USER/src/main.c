/*********************************************************************************************************************
* COPYRIGHT NOTICE
* Copyright (c) 2019,逐飞科技
* All rights reserved.
* 技术讨论QQ群：一群：179029047(已满)  二群：244861897
*
* 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
* 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
*
* @file				main
* @company			成都逐飞科技有限公司
* @author			逐飞科技(QQ3184284598)
* @version			查看doc内version文件 版本说明
* @Software			IAR 8.3 or MDK 5.24
* @Target core		MM32SPIN2XPs
* @Taobao			https://seekfree.taobao.com/
* @date				2020-11-23
********************************************************************************************************************/

#include "headfile.h"


// *************************** 例程说明 ***************************

// **************************** 宏定义 ****************************
// **************************** 宏定义 ****************************

// **************************** 变量定义 ****************************

// **************************** 变量定义 ****************************


// **************************** 代码区域 ****************************
int main(void)
{
    all_init();
	
//	   uart_init(UART_4,115200,UART4_TX_C10,UART4_RX_C11);
	lcd_init();
	lcd_init();

    while(flag_lcd)
    {
        LCD_display();
    }
		
		mpu6050_init();
		ultrasonic_init();
    while(flag_knock)
    {
        Encoder_Left  =  Encoder_Read(4);
        Encoder_Right =  -Encoder_Read(3);
		   	systick_delay_ms(2);
        if(Encoder_Left>20&&Encoder_Right>20)
        {
           flag_knock=0;
					 uart_putchar(UART_4,0x5A);
					 gpio_set(SEND_PORT_PIN,0);					    // 拉高使能 有去模块发送 实际接上电上拉就开始发送 程序可以不用控制
        }
    }

    my_timer();
//		
    while(1)
    {
			lcd_showfloat(20,1,ring_state,3,3);
      lcd_showfloat(20,2,ramp_state,3,3);
			lcd_showfloat(20,3,fork_state,3,3);
			lcd_showfloat(20,4,stop_state,3,3);
    }
}

