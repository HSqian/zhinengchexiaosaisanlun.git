/******************************************************************************
* 以下所有内容版权均属乾乾所有，未经允许不得用于商业用途
* @file			 adc_RTT
* @date
* @school    湖北工业大学
* @Software  MDK5 V5.32
* @author    hsq
* @version   V1.0
* @Target    CORE MM32
* @introduction adc线程，执行读取adc的值，adc归一化，以及差比和求值
******************************************************************************/
#include "adc_RTT.h"
#include <rthw.h>
#define THREAD_PRIORITY 2
#define THREAD_TIMESLICE 10
static char adc_stack[512];
struct rt_thread adc_RT;
void adc_entry(void *parameter)
{
	while (1)
	{
		rt_enter_critical();
		OR_Chevron_outR = adc_mean_filter(ADC_2, ADC2_CH04_A04, 5);
		OR_Horizontal_R = adc_mean_filter(ADC_2, ADC2_CH05_A05, 5);
		OR_Chevron_inR = adc_mean_filter(ADC_2, ADC2_CH06_A06, 5);
		OR_Chevron_inL = adc_mean_filter(ADC_2, ADC2_CH07_A07, 5); //采集原始值
		OR_Horizontal_L = adc_mean_filter(ADC_2, ADC2_CH14_C04, 5);
		OR_Chevron_outL = adc_mean_filter(ADC_2, ADC2_CH15_C05, 5);
		adc_nomalize(); //归一化

		/*    此處用環島的补偿标志位来做补偿处理*/
		if (Compensate_flag == 1)
		{
			dif_and_add = ADC_Compensate + 100 * err_get(Horizontal_L, Chevron_inL, Chevron_inR, Horizontal_R, Turn_A, Turn_B);
		}
		else
		{	
			//滑动均值滤波
			cur_tem = 100 * err_get(Horizontal_L, Chevron_inL, Chevron_inR, Horizontal_R, Turn_A, Turn_B); //差比和（差）求偏离程度
			dif_and_add = 0.5 * cur_tem + 0.5 * last_nine;
			last_nine = moving_filter(cur_tem);
		}
		rt_exit_critical();
		rt_thread_mdelay(2);
	}
}

void adcget_thread(void)
{
	my_adc_init();
	/* 创建线程1，名称是trol_RTT，入口是Camera_entry*/
	rt_thread_init(&adc_RT,
				   "adc_RT",
				   adc_entry,
				   RT_NULL,
				   &adc_stack[0],
				   sizeof(adc_stack),
				   THREAD_PRIORITY, THREAD_TIMESLICE);
	/* 如果获得线程控制块，启动这个线程 */
	rt_thread_startup(&adc_RT);
}
